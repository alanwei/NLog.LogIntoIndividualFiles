# NLog.LogIntoIndividualFiles

同一个运行程序内, 将不同类的日志输出到不同的文件中.

使用 `LoggerAttribute` 给类添加特性, 就会输出到对应的日志文件中. 具体效果参考[测试用例](https://github.com/Allen-Wei/NLog.LogIntoIndividualFiles/blob/master/NLog.LogIntoIndividualFilesTest/UnitTest1.cs).

## 安装

```
Install-Package NLog.LogIntoIndividualFiles
```
