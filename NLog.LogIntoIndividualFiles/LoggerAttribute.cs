﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFiles
{
    /// <summary>
    /// NLog 日志特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class LoggerAttribute : Attribute
    {
        /// <summary>
        /// 规则名称
        /// </summary>
        public String RuleName { get; private set; }
        /// <summary>
        /// 最大日志级别
        /// </summary>
        public LoggerLevel MaxLevel { get; set; }
        /// <summary>
        /// 最小日志级别
        /// </summary>
        public LoggerLevel MinLevel { get; set; }
        /// <summary>
        /// Target Name
        /// </summary>
        public String TargetName { get; set; }
        /// <summary>
        /// Target 文件名
        /// </summary>
        public String TargetFileName { get; set; }
        /// <summary>
        /// Target Layout
        /// </summary>
        public String TargetLayout { get; set; }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="ruleName">规则名称</param>
        public LoggerAttribute(String ruleName)
        {
            this.RuleName = ruleName;

            this.MaxLevel = LoggerLevel.Fatal;
            this.MinLevel = LoggerLevel.Trace;
            this.TargetName = this.RuleName;
            this.TargetFileName = $"${{basedir}}/logs/{this.RuleName}.${{shortdate}}.${{level}}.log";
            this.TargetLayout = "${longdate} ${uppercase:${level}} ${message}";
        }
    }
}
