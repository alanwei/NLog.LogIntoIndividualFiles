﻿using System;
using NLog;
using NLog.Targets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;
using NLog.Layouts;

namespace NLog.LogIntoIndividualFiles
{

    /// <summary>
    /// NLog.LogManager 扩展
    /// </summary>
    public static class LogManagerExt
    {
        private static object locker = new object();
        private static SortedSet<String> existedRules;

        static LogManagerExt()
        {
            if (LogManager.Configuration == null)
            {
                Trace.TraceWarning("LogManager.Configuration.LoggingRules is null");
                return;
            }
        }

        private static ILogger getNLogger(String ruleName)
        {
            LogManagerDebugger.Debug($"GetLogger({ruleName})");
            return String.IsNullOrWhiteSpace(ruleName) ? LogManager.GetCurrentClassLogger() : LogManager.GetLogger(ruleName);

        }

        /// <summary>
        /// 获取 ILogger 实例
        /// </summary>
        /// <returns></returns>
        public static ILogger GetLogger()
        {
            try
            {
                if (LogManager.Configuration == null)
                {
                    LogManagerDebugger.Debug("LogManager.Configuration.LoggingRules is null");
                    return getNLogger(null);
                }

                if (existedRules == null)
                    existedRules = new SortedSet<string>(LogManager.Configuration.LoggingRules.Select(l => l.LoggerNamePattern));

                StackTrace trace = new StackTrace(true);

                var attribute = trace.GetFrames()
                    .Select(frame => frame.GetMethod().DeclaringType)
                    .Distinct()
                    .Select(type =>
                    {
                        try
                        {
                            var attr = type.GetCustomAttribute<LoggerAttribute>(true);
                            LogManagerDebugger.Debug($"Type: {type.FullName}, Attribute Is Null: {attr == null}");
                            return attr;
                        }
                        catch (Exception ex)
                        {
                            LogManagerDebugger.Debug($"{ex.Message}");
                            return null;
                        }
                    })
                    .LastOrDefault(attr => attr != null);

                LogManagerDebugger.Debug($"LoggerAttribute is null: {attribute == null}");

                if (attribute == null) return getNLogger(null);

                LogManagerDebugger.Debug($"LoggerAttribute Rule Name: {attribute.RuleName}");

                lock (locker)
                {
                    if (!existedRules.Contains(attribute.RuleName))
                    {
                        LogManagerDebugger.Debug($"Add Rule: {attribute.RuleName}");

                        existedRules.Add(attribute.RuleName);

                        bool existTarget = LogManager.Configuration.AllTargets.Any(t => t.Name == attribute.TargetName);
                        if (!existTarget)
                        {
                            LogManagerDebugger.Debug($"Add Target: {attribute.TargetName}");

                            FileTarget target = new FileTarget
                            {
                                Name = attribute.TargetName,
                                FileName = attribute.TargetFileName,
                                Layout = new SimpleLayout(attribute.TargetLayout),
                                Encoding = Encoding.UTF8
                            };
                            LogManager.Configuration.AddTarget(attribute.RuleName, target);
                        }

                        LogManager.Configuration.AddRule(attribute.MinLevel.ToLevel(), attribute.MaxLevel.ToLevel(), attribute.RuleName, attribute.RuleName);
                        LogManager.ReconfigExistingLoggers();
                        LogManagerDebugger.Debug($"ReconfigExistingLoggers end");
                    }
                }
                return getNLogger(attribute.RuleName);
            }
            catch (Exception ex)
            {
                LogManagerDebugger.Debug($"LogManagerExt.GetLogger: {ex.Message}");
                return LogManager.GetCurrentClassLogger();
            }

        }
    }
}
