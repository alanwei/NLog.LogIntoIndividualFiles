﻿using System;

namespace NLog.LogIntoIndividualFiles
{
    /// <summary>
    /// LogManager调试类
    /// </summary>
    public static class LogManagerDebugger
    {
        static LogManagerDebugger()
        {
            Debugger = log => { };
        }
        /// <summary>
        /// 调试回调函数
        /// </summary>
        public static Action<String> Debugger { get; set; }


        /// <summary>
        /// 执行调试
        /// </summary>
        /// <param name="log"></param>
        public static void Debug(String log)
        {
            try
            {
                Debugger(log);
            }
            catch { }
        }
    }

}
