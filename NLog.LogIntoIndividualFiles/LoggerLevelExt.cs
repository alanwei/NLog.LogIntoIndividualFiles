﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFiles
{
    public static class LoggerLevelExt
    {
        public static LogLevel ToLevel(this LoggerLevel level)
        {
            return LogLevel.FromString(level.ToString());
        }
    }

}
