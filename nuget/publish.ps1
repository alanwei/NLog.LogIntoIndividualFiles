﻿Get-ChildItem -Filter *.nuspec | ForEach-Object -Process {  "nuget pack " +$_.FullName | Invoke-Expression};
Get-ChildItem -Filter *.nupkg | ForEach-Object -Process {  "nuget push " +$_.FullName + " -Source https://api.nuget.org/v3/index.json" | Invoke-Expression};
Get-ChildItem -Filter *.nupkg |  Remove-Item