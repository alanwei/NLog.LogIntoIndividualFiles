﻿using NLog.LogIntoIndividualFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFilesTest
{
    [Logger("JobOne")]
    public class Job1
    {
        public static void Run()
        {
            var name = "Alan";
            BusinessLayer.Print("Job1");
            var upper = name.ToUpper();
        }
    }
}
