﻿using NLog.LogIntoIndividualFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFilesTest
{
    [Logger("JobOne")]
    public class SameJob1
    {
        public static void Run()
        {
            BusinessLayer.Print("SameJob1");
        }
    }
}
