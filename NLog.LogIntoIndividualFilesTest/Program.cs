﻿using NLog.LogIntoIndividualFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace NLog.LogIntoIndividualFilesTest
{
    class Program
    {
        public static void Main(String[] args)
        {
#if DEBUG
            Console.WriteLine("Debug Mode");
#else
            Console.WriteLine("NOT Debug Mode");
#endif

            Timer t = new Timer()
            {
                Enabled = true,
                Interval = 1500
            };
            t.Elapsed += T_Elapsed;
            t.Start();
            var logger = LogManager.GetCurrentClassLogger();
            LogManagerDebugger.Debugger = log =>
            {
                Console.WriteLine(log);
            };
            Console.ReadKey();
        }

        private static void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            Job1.Run();
        }
    }
}
