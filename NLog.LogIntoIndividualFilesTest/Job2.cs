﻿using NLog.LogIntoIndividualFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFilesTest
{
    [Logger("JobTwo")]
    public class Job2
    {
        public static void Run()
        {
            BusinessLayer.Print("Job2");
        }
    }
}
