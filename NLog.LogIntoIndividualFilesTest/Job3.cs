﻿using NLog.LogIntoIndividualFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFilesTest
{
    [Logger("JobThree")]
    public class Job3
    {
        public static void Run()
        {
            BusinessLayer.Print("Job3");
        }
    }
}
