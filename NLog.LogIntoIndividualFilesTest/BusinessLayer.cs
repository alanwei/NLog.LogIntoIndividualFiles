﻿using NLog.LogIntoIndividualFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLog.LogIntoIndividualFilesTest
{
    public class BusinessLayer
    {
        public static void Print(String name)
        {
            LogManagerExt.GetLogger().Error("Hello " + name + ".");
        }
    }
}
